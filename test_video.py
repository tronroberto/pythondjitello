#!/usr/bin/env python3
import cv2
import numpy as np

import djitellopy as tellopy
import image_face_processing as ifp
from djitellopy import hud

IS_FACE_DETECTION = True

img = np.zeros((720, 960))
cv2.imshow('pilot', img)
tello = tellopy.Tello()

tello.connect()
tello.stream_on()
try:
    while cv2.waitKey(50) == -1:
        img = tello.get_frame()
        if img is not None:
            if IS_FACE_DETECTION:
                # faces = ifp.image_faces(img)
                faces = ifp.image_faces_dnn(img)
                ifp.image_faces_rectangle(img, faces)
            tello.hud_state(img)
            cv2.imshow('pilot', img)
        else:
            print('No frame')
finally:
    tello.end()
    cv2.destroyAllWindows()
