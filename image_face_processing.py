#!/usr/bin/env python3
"""
Utilities for handling face detection
"""
import os.path
from pickletools import uint8
import cv2
import numpy as np

#Path for xml file with cascade data (autoselect among candidate locations)
PATH_CASCADE_XML_LIST = [
    'haarcascade_frontalface_default.xml',
    '/usr/share/opencv/haarcascades/haarcascade_frontalface_default.xml',
    '/Users/tron/Documents/miniconda2/share/OpenCV/haarcascades/haarcascade_frontalface_default.xml'
]

#Path for files of the DNN models
# PATH_DNN = ''
PATH_PROTO = 'deploy.prototxt.txt'
PATH_MODEL = 'res10_300x300_ssd_iter_140000.caffemodel'

for PATH_CASCADE_XML in PATH_CASCADE_XML_LIST:
    if os.path.isfile(PATH_CASCADE_XML):
        break

#Load cascade into global object
FACE_CASCADE = cv2.CascadeClassifier(PATH_CASCADE_XML)

#Load RES10 300*300 DNN Model
net = cv2.dnn.readNetFromCaffe(PATH_PROTO, PATH_MODEL)


def image_faces_dnn(img):
    """ use a pre-trained dnn model to make the face detection work with face masks """
    # grab the frame dimensions and convert it to a blob
    (h, w) = img.shape[:2]
    blob = cv2.dnn.blobFromImage(cv2.resize(img, (300, 300)), 1.0,
        (300, 300), (104.0, 177.0, 123.0))
    # pass the blob through the network and obtain the detections and predictions
    net.setInput(blob)
    detections = net.forward()
    faces = []
    print(detections.shape[2])
    for i in range(0, detections.shape[2]):
        # extract the confidence (i.e., probability) associated with the
        # prediction
        confidence = detections[0, 0, i, 2]
        # filter out weak detections by ensuring the `confidence` is
        # greater than the minimum confidence
        if confidence < 0.5:
            continue
        # compute the (x, y)-coordinates of the bounding box for the
        # object
        box = detections[0, 0, i, 3:7] * np.array([w, h, w, h])
        (startX, startY, endX, endY) = box.astype("int")
        width = endX - startX
        height = endY - startY
        faces.append(tuple((int(startX),int(startY),int(width),int(height))))
    faces = np.array(faces)
    return faces

def image_faces(img):
    """ Runs face detection on the image, and returns list of bounding boxes """
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    faces = FACE_CASCADE.detectMultiScale(gray, 1.3, 5)
    return faces


def faces_largest(faces):
    """ Select the largest face in the list """
    area = -1.
    idx = -1
    for idx_current, (_, _, w_current, h_current) in enumerate(faces):
        area_current = w_current * h_current
        if area_current > area:
            area = area_current
            idx = idx_current
    return faces[idx]


def image_faces_rectangle(img, faces):
    """ Adds a green rectangle around each detected face """
    for (x, y, w, h) in faces:
        image_rectangle(img, x, y, w, h)
    return img


def image_rectangle(img, x, y, w, h):
    """ Adds a green rectangle to the image """
    cv2.rectangle(img, (x, y), (x + w, y + h), (0, 255, 0),2)
    return img

def rect_center(rect):
    '''
    Return the center of a rectangle
    '''
    (x, y, w, h) = rect
    return [x + w / 2, y + h / 2]
