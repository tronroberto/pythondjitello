#!/usr/bin/env python3
'''
Move the drone to center the largest face in the image at the center
'''

import math
from time import sleep

import cv2
import numpy as np

import image_face_processing as ifp
from djitellopy import Tello

FLAG_FLIGHT = True
CONTROL_GAINS = {'ud': -20, 'lr': 10, 'fb': -15}
CONTROL_THRESHOLD = 15


def rect_center(rect):
    '''
    Return the center of a rectangle
    '''
    (x, y, w, h) = rect
    return [x + w / 2, y + h / 2]


target_desired = [960 / 2, 720 / 3, 60]


def target_diff(target_current, direction):
    idx = ['lr', 'ud', 'fb'].index(direction)
    return (target_current[idx] - target_desired[idx])


img = np.zeros((500, 500))
cv2.imshow('pilot', img)

tello = Tello()

tello.connect()
sleep(1)
try:
    tello.print_state()
    # state = tello.get_state()
    # if state['bat'] <= 15:
    #     raise ValueError('Low battery')
    if FLAG_FLIGHT:
        for _ in range(5):
            ret = tello.takeoff(wait=True)
            if ret:
                break
        else:
            raise ValueError('Take off failed.')
        sleep(0.5)
        tello.move_up(50)

    tello.stream_on()
    while cv2.waitKey(50) == -1:
        img = tello.get_frame()
        if img is not None:
            message = f'Img: {img.shape[0]}x{img.shape[1]}'
            command = {'ud': 0, 'lr': 0, 'fb': 0}
            faces = ifp.image_faces(img)
            if len(faces) > 0:
                for (x, y, w, h) in faces:
                    message += f'[{w:>3}x{h:>3}@{str((x,y)):>10}]'
                ifp.image_faces_rectangle(img, faces)
                target_current = rect_center(ifp.faces_largest(faces)) + [w]
                message += f'. Tgt: cur{str(target_current):>19} des{str(target_desired):>19}'

                message += '. Cmd:'
                for direction in ['lr', 'ud', 'fb']:
                    error = target_diff(target_current, direction)
                    if abs(error) > CONTROL_THRESHOLD:
                        command[direction] = CONTROL_GAINS[
                            direction] * math.copysign(1, error)
            cv2.imshow('pilot', img)
            for direction in ['lr', 'ud', 'fb']:
                message += f" {direction}{command[direction]:>5}"
            if FLAG_FLIGHT:
                tello.send_rc_control(lr_vel=command['lr'],
                                      fb_vel=command['fb'],
                                      ud_vel=command['ud'],
                                      yaw_vel=0)
            print(message)
        else:
            print('No frame')
    tello.land()
finally:
    cv2.destroyAllWindows()
    tello.end()
