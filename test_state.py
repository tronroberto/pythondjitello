#!/usr/bin/env python3
'''
Test the state socket by connecting, getting the state once and then ending the
connection.
'''
from time import sleep

import cv2
import numpy as np

import djitellopy as tellopy

FLAG_RAW_STATE = False

img = np.zeros((200, 400))
cv2.putText(img, "Press any key here!", (40, 110), cv2.FONT_HERSHEY_SIMPLEX, 1,
            255)
cv2.imshow('State', img)
tello = tellopy.Tello()
tello.connect()

try:
    while cv2.waitKey(50) == -1:
        tello.print_state(FLAG_RAW_STATE)
        sleep(0.03)
finally:
    tello.end()
    cv2.destroyAllWindows()
