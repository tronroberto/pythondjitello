#!/usr/bin/env python3

import bu_audio

print('Testing notes (chromatic scale)')
bu_audio.test_notes()
print('Testing music (blue_danube.ogg)')
bu_audio.test_music()
print('Testing soundboard')
bu_audio.test_soundboard()
