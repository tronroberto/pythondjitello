import cv2 as cv
import numpy as np
from djitellopy import tello


# me = tello.Tello()
# me.connect()
# print(me.get_battery())
# me.streamoff()
# me.streamon()


lower_red = np.array([0, 43, 46])
upper_red = np.array([10, 255, 255])
lower_green = np.array([35, 43, 46])
upper_green = np.array([77, 255, 255])
lower_blue = np.array([100, 43, 46])
upper_blue = np.array([124, 255, 255])
# lower_yellow = np.array([20, 30, 30])
# upper_yellow = np.array([70, 255, 255])
# lower_purple = np.array([0, 0, 221])
# upper_purple = np.array([160, 30, 255])
# color_array = [[lower_red, upper_red], [
#     lower_green, upper_green], [lower_blue, upper_blue]]
color_array = [[lower_green, upper_green]]
capture1 = cv.VideoCapture(0)
font = cv.FONT_ITALIC


def getxy(image):
    hsv = cv.cvtColor(image, cv.COLOR_BGR2HSV)
    line = cv.getStructuringElement(cv.MORPH_RECT, (15, 15), (-1, -1))
    for (lower, upper) in color_array:
        mask = cv.inRange(hsv, lower, upper)
        mask = cv.morphologyEx(mask, cv.MORPH_OPEN, line)
        contours, hierarchy = cv.findContours(
            mask, cv.RETR_EXTERNAL, cv.CHAIN_APPROX_SIMPLE)
        index = -1
        max = 0
        for c in range(len(contours)):
            area = cv.contourArea(contours[c])
            if area > max:
                max = area
                index = c
        coor = [-1, -1]
        if index >= 0:
            rect = cv.minAreaRect(contours[index])
            cv.ellipse(image, rect, (0, 0, 255), 2, 8)
            cv.circle(image, (np.int32(rect[0][0]), np.int32(
                rect[0][1])), 2, (255, 0, 0), 2, 8, 0)
            # cv.putText(image, 'Detected', (50, 150), cv.FONT_HERSHEY_COMPLEX, 5, (0, 0, 255), 12)
            coor = [np.int32(rect[0][0]), np.int32(rect[0][1])]
    return [coor, image]


# while(True):
#     # ret, frame = capture1.read()
#     # cv.imshow("video-input", frame)
#     img = me.get_frame_read().frame
#     img = cv.resize(img, (1280, 720))
#     result = process(img)
#     cv.imshow("result", result)
#     c = cv.waitKey(50)
#     print(c)
#     if c == 27:  # ESC
#         break
