from djitellopy import tello
import KeyPressModule as kp
from time import sleep
import cv2
import pygame
import numpy as np


kp.init()

me = tello.Tello()

me.connect()

print(me.get_battery())

me.streamon()

window = pygame.display.set_mode((1280, 720))


def cv2ImageToSurface(cv2Image):
    # copy from https://www.py4u.net/discuss/139710
    if cv2Image.dtype.name == 'uint16':
        cv2Image = (cv2Image / 256).astype('uint8')
    size = cv2Image.shape[1::-1]
    if len(cv2Image.shape) == 2:
        cv2Image = np.repeat(cv2Image.reshape(size[1], size[0], 1), 3, axis=2)
        format = 'RGB'
    else:
        format = 'RGBA' if cv2Image.shape[2] == 4 else 'RGB'
        cv2Image[:, :, [0, 2]] = cv2Image[:, :, [2, 0]]
    surface = pygame.image.frombuffer(cv2Image.flatten(), size, format)
    return surface.convert_alpha() if format == 'RGBA' else surface.convert()


def getKeyboardInput():

    lr, fb, ud, yv = 0, 0, 0, 0

    speed = 50

    if kp.getKey("LEFT"):
        lr = -speed

    elif kp.getKey("RIGHT"):
        lr = speed

    if kp.getKey("UP"):
        fb = speed

    elif kp.getKey("DOWN"):
        fb = -speed

    if kp.getKey("w"):
        ud = speed

    elif kp.getKey("s"):
        ud = -speed

    if kp.getKey("a"):
        yv = -speed

    elif kp.getKey("d"):
        yv = speed

    if kp.getKey("q"):
        me.land()
        sleep(3)

    if kp.getKey("e"):
        me.takeoff()

    return [lr, fb, ud, yv]


while True:

    vals = getKeyboardInput()

    img = me.get_frame_read().frame
    img = cv2.resize(img, (1280, 720))
    pygameSurface1 = cv2ImageToSurface(img)
    window.blit(pygameSurface1, (0, 0))

    me.send_rc_control(vals[0], vals[1], vals[2], vals[3])
