import color_detection as cd
import cv2
from djitellopy import tello
import time
from time import sleep

me = tello.Tello()
me.connect()
print(me.get_battery())
me.streamoff()
me.streamon()

size = (1280, 720)
threshold = [200, 200]

me.takeoff()
sleep(3)
start = time.perf_counter()
while (time.perf_counter() < (start + 40)):
    img = me.get_frame_read().frame
    img = cv2.resize(img, (1280, 720))
    [xy, img] = cd.getxy(img)
    x = xy[0]
    cv2.imshow("Image", img)
    cv2.waitKey(1)
    print(x)
    if (x > 0 and x < 440):
        me.send_rc_control(0, 0, 0, -50)
    elif (x > 840 and x < 1280):
        me.send_rc_control(0, 0, 0, 50)
    else:
        me.send_rc_control(0, 0, 0, 0)
me.land()
