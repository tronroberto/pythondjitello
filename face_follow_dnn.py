#!/usr/bin/env python3
from time import sleep

import cv2

import controller as ctrl
import image_face_processing as ifp
from djitellopy import Tello

FLAG_FLIGHT = True  # True for fly; False for not flying
target_desired = [960 / 2, 300, 95]
error_prev = {'ud': 0, 'lr': 0, 'fb': 0}
arrow_factor = {'ud': -10, 'lr': 10, 'fb': -3}


def format_target(target):
    '''
    Format a target list (x,y,width) into a fixed-width string
    '''
    return f'({target[0]:3.0f},{target[1]:3.0f}x{target[2]:3.0f}px)'


tello = Tello()
tello.connect()
sleep(1)
try:

    tello.print_state()
    if FLAG_FLIGHT:
        for _ in range(5):
            ret = tello.takeoff(wait=True)
            if ret:
                break
        else:
            raise ValueError('Take off failed.')
        sleep(0.5)
        tello.move_up(60)

    # loop over the frames from the video stream
    tello.stream_on()
    while cv2.waitKey(50) == -1:
        img = tello.get_frame()
        if img is not None:
            message = f'Img: {img.shape[0]}x{img.shape[1]}'
            command = {'ud': 0, 'lr': 0, 'fb': 0}
            faces = ifp.image_faces_dnn(img)
            if len(faces) > 0:
                for (x, y, w, h) in faces:
                    message += f'[{w:3d}x{h:3d}@({x:3d},{y:3d})]'

                ifp.image_faces_rectangle(img, faces)
                target_current = ifp.rect_center(
                    ifp.faces_largest(faces)) + [w]
                message += f'. Tgt: cur{format_target(target_current)} des{format_target(target_desired)}'
                message += '. Cmd:'
                command, error_prev = ctrl.controller(target_current,
                                                      target_desired,
                                                      error_prev)
                coords_desired = (int(target_desired[0]),
                                  int(target_desired[1]))
                coords_head = (int(coords_desired[0] +
                                   command['lr'] * arrow_factor['lr']),
                               int(coords_desired[1] +
                                   command['ud'] * arrow_factor['ud']))
                coords_head_fb = (coords_desired[0],
                                  int(coords_desired[1] +
                                      command['fb'] * arrow_factor['fb']))

                cv2.arrowedLine(img, coords_desired, coords_head, (0, 0, 255),
                                4)
                cv2.arrowedLine(img, coords_desired, coords_head_fb,
                                (255, 255, 0), 4)

            for direction in ['lr', 'ud', 'fb']:
                message += f" {direction}{command[direction]:+7.2f}"
            if FLAG_FLIGHT:
                tello.send_rc_control(lr_vel=command['lr'],
                                      fb_vel=command['fb'],
                                      ud_vel=command['ud'],
                                      yaw_vel=0)
            print(message)
            tello.hud_state(img)
            cv2.imshow('pilot', img)
        else:
            print('No frame')
    if FLAG_FLIGHT:
        for count in range(0, 5):
            result = tello.land()
            if result:
                break

finally:
    # clean up the camera and close opened windows
    tello.end()
    cv2.destroyAllWindows()
