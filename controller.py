#!/usr/bin/env python3
# this PD controller is used to generate the control command
# that moves the drone to the target position
K_p = {'ud': -0.20, 'lr': 0.1, 'fb': -0.50}
K_i = {'ud': -0.1, 'lr': 0.1, 'fb': -0.0}
CONTROL_THRESHOLD = 10

# error_prev = {'ud': 0, 'lr': 0, 'fb': 0}
# target_desired = [960/2, 720/3, 100]


# calculate the diffencne with respect to a certain direction
def target_diff(target_current, target_desired, direction):
    idx = ['lr', 'ud', 'fb'].index(direction)
    return (target_current[idx] - target_desired[idx])


def controller(target_current, target_desired, error_prev):
    command = {'ud': 0, 'lr': 0, 'fb': 0}
    Error = {'ud': 0, 'lr': 0, 'fb': 0}
    for direction in ['lr', 'ud', 'fb']:
        error = target_diff(target_current, target_desired, direction)
        if abs(error) > CONTROL_THRESHOLD:
            command[direction] = K_p[direction] * error
            +K_i[direction] * (error - error_prev[direction])
            Error[direction] = error
        else:
            command[direction] = 0
            Error[direction] = 0

    return command, Error
