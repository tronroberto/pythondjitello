#!/usr/bin/env python3
from time import sleep

from djitellopy import Tello

tello = Tello()

tello.connect()
tello.mission_pads_on()
try:
    tello.print_state()
    tello.takeoff()
    for i in range(0, 4):
        tello.move_up(30)
        sleep(1)
        tello.rotate_ccw(90)
        sleep(1)
        tello.move_down(30)
        sleep(1)
    tello.land()
finally:
    tello.end()
